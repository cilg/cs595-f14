
static char help[] = " \n";

//#include <stdlib.h>
//#include <iostream>
//#include <stdio.h>
//#include <time.h>
//#include <math.h>
#include <petscts.h>

const int MAX=10, MASS = 1, SIGMA = 1, EPSILON = 1; //MAX is max # of molecules, SIGMA and EPSILON are coefficients in LJ potential.
const double DT=0.05, TMAX = 0.5; //DT is the time step interval, TMAX is the max amount of time you want to simulate over.

typedef struct { double x; double y;} positions;   //position
typedef struct { double x; double y;} velocitys;   //velocity
typedef struct { double x; double y;} accelerations; //acceleration
typedef struct { double x; double y;} momentums;     //momentum

positions pos[MAX];
velocitys vel[MAX];
accelerations accel[MAX];
momentums p[MAX];

void initialize(){

  for(int i =0; i< MAX; i++){
     pos[i].x = 0;
     pos[i].y = 0;
     vel[i].x = 0;
     vel[i].y = 0;
     accel[i].x = 0;
     accel[i].y = 0;
     p[i].x = 0;
     p[i].y = 0;
  }
}

double force(double x1, double y1, double x2, double y2){
  int r, f;
  r = sqrt((x1-x2)^2+(y1-y2)^2);
  rx = (x1-x2)/r;
  r = max(r,SIGMA);
  fx = 4 * EPSILON * rx * ((12*pow(SIGMA,12)/pow(r,13)-6*pow(SIGMA,6)/pow(r,7)));
  return fx;
}

double distance(double Vx, double Accx,double &PosX,)
{
  double disX;
  disX = Vx * DT + (0.5 * Accx * DT*DT);
  PosX = PosX + disX;
  return PosX;
}

double velocity (double &Vx,double Accx)
{
  double val;
  int check =0;
  //x direction
  val = Vx*Vx + (2 * Accx * DT);
  if (val < 0)
   {
    val = val * -1;
    check = 1;
   }
  Vx = sqrt (val);
  if (check ==1)
   { Vx = Vx * -1; }
  return Vx;
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **argv)
{
  srand(time(NULL));
  int  NumParticles, xmin=0, xmax=1, ymin=0, ymax=1, time;
  double xpos[MAX][time], ypos[MAX][time], xacc[MAX][time], xvel[MAX][time], yvel[MAX][time], yacc[MAX][time];
  //double vel[MAX][time], acc[MAX][time];
  float a, b; //dummy variable
  double Fx=0, Fy=0, t=0; //Force in x direction and y direction, t is time elapsed, initialized to 0.

  FILE *fp;

  PetscInitialize(&argc,&argv,(char*)0,help);

  //set initial conditions
  for (int i=0; i<MAX; i++)
     {  //position
       a = rand()%5+1;
       a = a/10;
       //where did pos come frome
       pos[i].x = a;
       // xpos[i][0] = x; not deleting in case structs fail mmiserably
       b =  rand()%5+1;
       b = b/10;
       pos[i].y = b;
       // ypos[i,0] = y;

       //velocity
       a = rand()%7+1;
       vel[i].x = a;
       b = rand()%7+1;
       vel[i].y = b;
     }


  while (t<TMAX)  //time-stepping loop
    {
      for(int i=0; i<MAX; i++){                   //calculates accel for particles in x and y direction
        Fx=0;
        Fy=0;
        for(int j=0; j<MAX; j++){
          if(i==j){}
          else{
            Fx = Fx + force(pos[i].x,pos[i].y,pos[j].x,pos[j].y);
            Fy = Fy + force(pos[i].y,pos[i].x,pos[j].y,pos[j].x);
          }
          accel[i].x=Fx;
          accel[i].y=Fy;
        }
      }	
      for(int i=0; i<MAX; i++){        //calculates new velocity
        vel[i].x = vel[i].x + velocity(vel[i].x,accel[i].x);
        vel[i].y = vel[i].y + velocity(vel[i].y,accel[i].y);
      }
      
      for(int i=0; i<MAX; i++){        //calculates new position
        pos[i].x=distance(vel[i].x,accel[i].x,pos[i].x);
        pos[i].y=distance(vel[i].y,accel[i].y,pos[i].y);
        
        fp = fopen ("list_initial" , "w" );
        fprintf(fp, "%d %d %d\n",i,pos[i].x,pos[i].y);    //took code from other "online" version to write to a file unsure if works
        fclose(fp);
      }
      
      t = t + DT;
    }




  PetscFunctionReturn(0);
  //return (0);
}
