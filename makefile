
CFLAGS	        =
FFLAGS	        =
CPPFLAGS        =
FPPFLAGS        =
LOCDIR          = src/ts/examples/tutorials/
EXAMPLESC       = ex1.c ex2.c ex3.c ex4.c ex5.c ex6.c ex7.c ex8.c \
                ex9.c ex10.c ex11.c ex12.c ex13.c ex14.c ex15.c ex16.c ex17.c \
                ex19.c ex20.c ex21.c ex22.c ex23.c ex24.c ex25.c ex26.c \
                ex28.c ex30.cxx ex31.c ex40.c ex41.c
EXAMPLESF       = ex1f.F ex2f.F ex22f.F # ex22f_mf.F90
EXAMPLESFH      = ex2f.h
MANSEC          = TS
DIRS            = phasefield advection-diffusion-reaction eimex

include ${PETSC_DIR}/conf/variables
include ${PETSC_DIR}/conf/rules

CarlyIlgVersionOfFinalProj: CarlyIlgVersionOfFinalProj.o  chkopts
	-${CLINKER} -o CarlyIlgVersionOfFinalProj CarlyIlgVersionOfFinalProj.o  ${PETSC_TS_LIB}
	${RM} CarlyIlgVersionOfFinalProj.o

ex12: ex12.o  chkopts
	-${CLINKER} -o ex12 ex12.o  ${PETSC_TS_LIB}
	${RM} ex12.o
include ${PETSC_DIR}/conf/test
